# Créé par Gauthier
import tkinter # importation de la bibliothèque tkinter
from tkinter import ttk # importation de ttk de la bibliothèque tkinter
import gestionnaire_de_participants # importation de la bibliothèque gestionnaire_de_participants
import gestionnaire_d_equipes # importation de la bibliothèque gestionnaire_d_equipe
import gestionnaire_de_matchs # importation de la bibliothèque gestionnaire_de_matchs
import os # importation de la bibliothèque OS

# Créé par Gauthier et modifié par Quentin
def start_app(): # définition de la fonction start_app
    try: # on essaye
        select = start_app_button.selection_get() # création d'une variable select
        if select == liste_menu[0]: # si select est égal au premier élément de la liste déroulante du menu
            gestionnaire_de_participants.window_gestion_tournoi() # lancer le gestionnaire de tournoi
        elif select == liste_menu[1]: # sinon si select est égal au deuxième élément de la liste déroulante du menu
            gestionnaire_d_equipes.window_gestion_equipe() # lancer le gestionnaire d'équipe
        else: # sinon
            # gestionnaire_de_matchs.window_gestion_matchs() # lancer le gestionnaire de matchs
            os.system('gestionnaire_de_matchs.py')

    except: # si on a une erreur
        message_error = tkinter.Label(main_window, text="Sélectionner une option dans\nla liste déroulante", font="Arial 8", width=40, background='white') # affiche un message d'erreur
        message_error.place(relx=0.55, rely=0.80) # place le message d'erreur

# Créé par Gauthier
main_window = tkinter.Tk() # création d'une variable main_window (la fenêtre du menu)
main_window.title("GTS | Accueil") # changement du titre de la fenêtre
main_window.geometry("500x500") # changement de la taille de la fenêtre
main_window.resizable(width=False, height=False) # rend impossible le changement de taille de la fenêtre
bg = tkinter.PhotoImage(file="icon.png") # création d'une variable bg contenant l'image de fond importée depuis un fichier
test = tkinter.Label(main_window, image=bg) # mise en place de l'image dans la fenêtre
test.place(relx=0.5, rely=0.5, anchor="center") # alignement au centre
main_window_title = tkinter.Label(main_window, text="Gestionnaire de tournoi sportif", font="Arial 15 bold", bg='white') # création d'une variable main_window_title contenant le titre du programme
main_window_title.place(relx=0.5, rely=0.05, anchor="center") # alignement du titre
liste_menu = ["Gestion des participants", "Gestion des équipes", "Gestion des matchs"] # création d'une variable liste_meun contenant une liste des appliactions disponibles au lancement
main_window_menu = ttk.Combobox(main_window, values=liste_menu, state="readonly") # création d'une variable bg contenant un combobox (liste déroulante) avec dedans les valeurs de liste_menu
main_window_menu.place(relx=0.8, rely=0.55, anchor="center") # alignement du combobox
start_app_button = tkinter.Button(main_window, text="Lancer l'application", command=start_app) # création d'une variable bg contenant un boutan appellant la fonction srtat_app au click
start_app_button.place(relx=0.8, rely=0.70, anchor="center") # alignement du bouton

main_window.mainloop() # lancement de la fenêtre