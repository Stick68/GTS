# Créé Aréna
from tkinter import *
import numpy as np
import matplotlib.pyplot as plt

# Créé par Aréna et modifié par Quentin
def stats_physique():
    """Affiche un diagramme circulaire représentant les statistiques physiques d'un joueur"""
    categories = ['Age', 'Taille', 'Poids', 'Vitesse']
    categories = [*categories, categories[0]]  # ajoute l'âge deux fois afin de boucler correctement

    # Récupération des informations rentrées par l'utilisateur
    stats_physique = [int(age_input.get()), int(taille_input.get()), int(poids_input.get()), int(vitesse_input.get())]
    stats_physique = [*stats_physique, stats_physique[0]]  # ajoute l'âge deux fois afin de boucler correctement

    label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(stats_physique))

    plt.figure(figsize=(8, 8))
    plt.subplot(polar=True)
    plt.plot(label_loc, stats_physique)

    plt.title('Statistiques joueur', size=20, y=1.05)
    lines, labels = plt.thetagrids(np.degrees(label_loc), labels=categories)

    plt.show()

# Créé par Aréna et modifié par Quentin
def stats_matchs():
    """Affiche un diagramme circulaire représentant les performances en matchs d'un joueur."""
    categories2 = ['Victoires', 'Défaites', 'Fautes']
    categories2 = [*categories2, categories2[0]]  # ajoute le nombre de victoires deux fois afin de boucler correctement

    # Récupération des informations rentrées par l'utilisateur
    Stats_values = [nombre_victoire_input.get(), nombre_defaites_input.get(), nombre_fautes_input.get()]
    Stats_values = [*Stats_values, Stats_values[0]]  # ajoute le nombre de victoires deux fois afin de boucler correctement

    label_loc = np.linspace(start=0, stop=2 * np.pi, num=len(Stats_values))

    plt.figure(figsize=(8, 8))
    plt.subplot(polar=True)
    plt.plot(label_loc, Stats_values)

    plt.title('Statistiques en match', size=20, y=1.05)
    lines, labels = plt.thetagrids(np.degrees(label_loc), labels=categories2)

    plt.show()

# Créé par Gauthier et modifié par Quentin, Aréna
def window_afficher_stats(joueur):
    """Crée une nouvelle fenêtre contenant toutes les informations relatives aux statistiques du joueur donné en paramètre."""
    global root, taille_input, poids_input, vitesse_input, age_input, nombre_fautes_input, nombre_victoire_input, nombre_defaites_input
    root = Tk()
    root.title("GTS | Visualisateur de statistiques")
    root.geometry("500x400")

    title = Label(root, text="Statistiques du joueur", font=("Arial", 14, "bold"))
    title.place(relx=0.5, rely=0.05, anchor="center")

    # Widgets relatifs aux informations générales concernant le joueur
    nom = Label(root, text="Nom du joueur :", font="Arial 10")
    nom.place(relx=0.3, rely=0.15, anchor="center")
    nom_input = Entry(root)
    nom_input.insert(0, joueur)
    nom_input.place(relx=0.6, rely=0.15, anchor="center")

    taille = Label(root, text="Taille du joueur (en cm) :", font="Arial 10")
    taille.place(relx=0.3, rely=0.25, anchor="center")
    taille_input = Entry(root)
    taille_input.insert(0, '132')
    taille_input.place(relx=0.6, rely=0.25, anchor="center")

    poids = Label(root, text="Poids du joueur (en kg) :", font="Arial 10")
    poids.place(relx=0.3, rely=0.35, anchor="center")
    poids_input = Entry(root)
    poids_input.insert(0, '230')
    poids_input.place(relx=0.6, rely=0.35, anchor="center")

    vitesse = Label(root, text="Vitesse du joueur (en km/h) :", font="Arial 10")
    vitesse.place(relx=0.3, rely=0.45, anchor="center")
    vitesse_input = Entry(root)
    vitesse_input.insert(0, '09')
    vitesse_input.place(relx=0.6, rely=0.45, anchor="center")

    age = Label(root, text="Age du joueur :", font="Arial 10")
    age.place(relx=0.3, rely=0.55, anchor="center")
    age_input = Entry(root)
    age_input.insert(0, '17')
    age_input.place(relx=0.6, rely=0.55, anchor="center")

    nombre_victoire = Label(root, text="Nombre de victoires :", font="Arial 10")
    nombre_victoire.place(relx=0.3, rely=0.65, anchor="center")
    nombre_victoire_input = Entry(root)
    nombre_victoire_input.insert(0, '0')
    nombre_victoire_input.place(relx=0.6, rely=0.65, anchor="center")
    
    nombre_defaites = Label(root, text="Nombre de défaites :", font="Arial 10")
    nombre_defaites.place(relx=0.3, rely=0.75, anchor="center")
    nombre_defaites_input = Entry(root)
    nombre_defaites_input.insert(0, '250')
    nombre_defaites_input.place(relx=0.6, rely=0.75, anchor="center")
    
    nombre_fautes = Label(root, text="Nombre de fautes :", font="Arial 10")
    nombre_fautes.place(relx=0.3, rely=0.85, anchor="center")
    nombre_fautes_input = Entry(root)
    nombre_fautes_input.insert(0, '0')
    nombre_fautes_input.place(relx=0.6, rely=0.85, anchor="center")
    
    
    voir = Button(root, text="Visualiser les statistiques du joueur",command=stats_physique)
    voir.place(relx=0.3, rely=.95, anchor="center")

    voir2 = Button(root, text="Visualiser les statistiques en match",command=stats_matchs)
    voir2.place(relx=0.7, rely=.95, anchor="center")
    
    root.mainloop()

if __name__ == '__main__':
    window_afficher_stats('David Schirmer')
