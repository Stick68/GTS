# Créé par Timéo
import tkinter 
import sqlite3

# Créé par Timéo
def draw_samples (canvas):
    """Fonction pour dessiner les lignes du tournoi sur le canvas"""
    for i in range(int(int(nb_equipe)/2)):
        y=i*150
        x=1
        if (nb_equipe>2) and  i==0:
            canvas.create_line ((x+500, y+175), (x+550, y+175), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((x+500, y+475), (x+550, y+475), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((x+550, y+175), (x+550, y+475), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((x+550, y+300), (x+600, y+300), 
                                fill="black", width=5, smooth=True,)
            
            canvas.create_line ((800-x, y+175), (750-x, y+175), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((800-x, y+475), (750-x, y+475), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((750-x, y+175), (750-x, y+475), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((750-x, y+400), (700-x, y+400), 
                                fill="black", width=5, smooth=True,)
        if (nb_equipe>4) and (i%2)==0 :
            canvas.create_line ((x+300, y+100), (x+350, y+100), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((x+300, y+250), (x+350, y+250), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((x+350, y+100), (x+350, y+250), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((x+350, y+175), (x+400, y+175), 
                                fill="black", width=5, smooth=True,)
            
            canvas.create_line ((1000-x, y+100), (950-x, y+100), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((1000-x, y+250), (950-x, y+250), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((950-x, y+100), (950-x, y+250), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((950-x, y+175), (900-x, y+175), 
                                fill="black", width=5, smooth=True,)
        if nb_equipe==16 and ((i%2)==1 or i==0 or i==2):
            canvas.create_line ((x+100, y+50), (x+150, y+50), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((x+150, y+50), (x+150, y+150), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((x+100, y+150), (x+150,y+ 150), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((x+150, y+100), (x+200, y+100), 
                                fill="black", width=5, smooth=True,)
            
            canvas.create_line ((1200-x, y+50), (1150-x, y+50), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((1150-x, y+50), (1150-x, y+150), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((1200-x, y+150), (1150-x, y+150), 
                                fill="black", width=5, smooth=True,)
            canvas.create_line ((1150-x, y+100), (1100-x, y+100), 
                                fill="black", width=5, smooth=True,)
            
# Créé par Timéo
def reactualiser():
    """Fonction pour réinitialiser les boutons et le tournoi"""
    global lst_equipedata,lst_equipe,nb_equipe,btnwin,btn1_1,btn1_2,btn2_1,btn2_2,btn2_3,btn2_4,btn3_1,btn3_2,btn3_3,btn3_4,btn3_5,btn3_6,btn3_7,btn3_8,btn4_1,btn4_2,btn4_3,btn4_4,btn4_5,btn4_6,btn4_7,btn4_8,btn4_9,btn4_10,btn4_11,btn4_12,btn4_13,btn4_14,btn4_15,btn4_16
    lst_equipe=lst_equipedata.copy()
    if nb_equipe==4:
        btn2_txt='Equipe'
    if nb_equipe==8:
        btn2_txt='Demi-finaliste'
        btn3_txt='Equipe'
    if nb_equipe==16:
        btn2_txt='Demi-finaliste'
        btn3_txt='Quart-finaliste'
        btn4_txt='Equipe'
    btnwin=tkinter.Button(canvas, text="Gagnant", bg='yellow', command=fbtnwin, width=15, height=1 )
    canvas.create_window(650, 100, window=btnwin)
    btn1_1=tkinter.Button(canvas, text="Finaliste", command=fbtn1_1, width=15, height=1 )
    canvas.create_window(650, 300, window=btn1_1)
    btn1_2=tkinter.Button(canvas, text="Finaliste", command=fbtn1_2, width=15, height=1 )
    canvas.create_window(650, 400, window=btn1_2)
    if nb_equipe>=4:
        btn2_1=tkinter.Button(canvas, text=btn2_txt, command=fbtn2_1, width=15, height=1 )
        canvas.create_window(450, 175, window=btn2_1)
        btn2_2=tkinter.Button(canvas, text=btn2_txt, command=fbtn2_2, width=15, height=1 )
        canvas.create_window(450, 475, window=btn2_2)
        btn2_3=tkinter.Button(canvas, text=btn2_txt, command=fbtn2_3, width=15, height=1 )
        canvas.create_window(850, 175, window=btn2_3)
        btn2_4=tkinter.Button(canvas, text=btn2_txt, command=fbtn2_4, width=15, height=1 )
        canvas.create_window(850, 475, window=btn2_4)
    if nb_equipe>=8:
        btn3_1=tkinter.Button(canvas, text=btn3_txt, command=fbtn3_1, width=15, height=1 )
        canvas.create_window(250, 100, window=btn3_1)
        btn3_2=tkinter.Button(canvas, text=btn3_txt, command=fbtn3_2, width=15, height=1 )
        canvas.create_window(250, 250, window=btn3_2)
        btn3_3=tkinter.Button(canvas, text=btn3_txt, command=fbtn3_3, width=15, height=1 )
        canvas.create_window(250, 400, window=btn3_3)
        btn3_4=tkinter.Button(canvas, text=btn3_txt, command=fbtn3_4, width=15, height=1 )
        canvas.create_window(250, 550, window=btn3_4)
        btn3_5=tkinter.Button(canvas, text=btn3_txt, command=fbtn3_5, width=15, height=1 )
        canvas.create_window(1050, 100, window=btn3_5)
        btn3_6=tkinter.Button(canvas, text=btn3_txt, command=fbtn3_6, width=15, height=1 )
        canvas.create_window(1050, 250, window=btn3_6)
        btn3_7=tkinter.Button(canvas, text=btn3_txt, command=fbtn3_7, width=15, height=1 )
        canvas.create_window(1050, 400, window=btn3_7)
        btn3_8=tkinter.Button(canvas, text=btn3_txt, command=fbtn3_8, width=15, height=1 )
        canvas.create_window(1050, 550, window=btn3_8)
    if nb_equipe==16:
        btn4_1=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_1, width=15, height=1 )
        canvas.create_window(50, 50, window=btn4_1)
        btn4_2=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_2, width=15, height=1 )
        canvas.create_window(50, 150, window=btn4_2)
        btn4_3=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_3, width=15, height=1 )
        canvas.create_window(50, 200, window=btn4_3)
        btn4_4=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_4, width=15, height=1 )
        canvas.create_window(50, 300, window=btn4_4)
        btn4_5=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_5, width=15, height=1 )
        canvas.create_window(50, 350, window=btn4_5)
        btn4_6=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_6, width=15, height=1 )
        canvas.create_window(50, 450, window=btn4_6)
        btn4_7=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_7, width=15, height=1 )
        canvas.create_window(50, 500, window=btn4_7)
        btn4_8=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_8, width=15, height=1 )
        canvas.create_window(50, 600, window=btn4_8)
        btn4_9=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_9, width=15, height=1 )
        canvas.create_window(1250, 50, window=btn4_9)
        btn4_10=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_10, width=15, height=1 )
        canvas.create_window(1250, 150, window=btn4_10)
        btn4_11=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_11, width=15, height=1 )
        canvas.create_window(1250, 200, window=btn4_11)
        btn4_12=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_12, width=15, height=1 )
        canvas.create_window(1250, 300, window=btn4_12)
        btn4_13=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_13, width=15, height=1 )
        canvas.create_window(1250, 350, window=btn4_13)
        btn4_14=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_14, width=15, height=1 )
        canvas.create_window(1250, 450, window=btn4_14)
        btn4_15=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_15, width=15, height=1 )
        canvas.create_window(1250, 500, window=btn4_15)
        btn4_16=tkinter.Button(canvas, text=btn4_txt, command=fbtn4_16, width=15, height=1 )
        canvas.create_window(1250, 600, window=btn4_16)

# Créé par Timéo
def quitter():
    """Fonction pour quitter l'application"""
    fenetre1.destroy()

# Créé par Timéo
def Valider():
    """Fonction pour valider le choix de l'équipe"""
    global lst_equipe,equipes,liste,condition,gagnant,perdant,color
    equipes.destroy()
    condition=True
    if gagnant.get()==1:
        color='green'
    if perdant.get()==1:
        color='red'
    if (gagnant.get()==1 and perdant.get()==1) or (gagnant.get()==0 and perdant.get()==0):
        color='grey'
    fenetre1.quit()
    return equipe_choisi
          
# Créé par Timéo          
def clic(evt):
    """Fonction appelée lorsqu'une équipe est sélectionnée dans la liste"""
    global equipe_choisi
    i=liste.curselection()  ## Récupération de l'index de l'élément sélectionné
    equipe_choisi=liste.get(i)  ## On retourne l'élément (un string) sélectionné
 
 # Créé par Timéo
def choisir_equipe(lst_choix):
    """Fonction pour afficher la liste des équipes et permettre à l'utilisateur de choisir"""
    global equipe_choisi,liste,equipes,gagnant,perdant,color
    print(lst_choix)
    equipes=tkinter.Toplevel(fenetre1)
    equipes.title ("liste des equipes")
    var = tkinter.Variable(value=lst_choix)
    print(var)
    liste = tkinter.Listbox(equipes,listvariable=var , selectmode=tkinter.SINGLE)
    liste.pack(expand=True, fill=tkinter.BOTH, side=tkinter.LEFT)
    liste.bind('<ButtonRelease-1>',clic)
    btn_valider=tkinter.Button(equipes, text="Valider", command=Valider, width=15, height=1 )
    btn_valider.pack()
    gagnant = tkinter.IntVar()
    cochewin=tkinter.Checkbutton(equipes, text="gagnant", variable=gagnant, onvalue = 1, offvalue = 0)
    cochewin.pack()
    perdant = tkinter.IntVar()
    cochelose=tkinter.Checkbutton(equipes, text="perdant", variable=perdant, onvalue = 1, offvalue = 0)
    cochelose.pack()
# Créé par Timéo
# Fonctions pour chaque bouton du tournoi (gagnant, finaliste, demi-finaliste, quart de finaliste)
def fbtnwin():
    global condition
    if btn1_1['bg']=='green':
        equipe_choisi=btn1_1['text']
    if btn1_2['bg']=='green':
        equipe_choisi=btn1_2['text']
    btnwin=tkinter.Button(canvas, text=equipe_choisi, bg='yellow', command=fbtnwin, width=15, height=1 )
    canvas.create_window(650, 100, window=btnwin)
    fenetre1.mainloop()
def fbtn1_1():
    global condition,btn1_1,nb_equipe
    if nb_equipe<4:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn2_1['text'], btn2_2['text']])
    equipes=tkinter.mainloop()
    btn1_1=tkinter.Button(canvas, text=equipe_choisi, command=fbtn1_1, bg=color, width=15, height=1 )
    canvas.create_window(650, 300, window=btn1_1)
    print(btn2_1['text'])
    fenetre1.mainloop()
def fbtn1_2():
    global condition,btn1_2,nb_equipe
    if nb_equipe<4:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn2_3['text'], btn2_4['text']])
    equipes=tkinter.mainloop()
    btn1_2=tkinter.Button(canvas, text=equipe_choisi, command=fbtn1_2, bg=color, width=15, height=1 )
    canvas.create_window(650, 400, window=btn1_2)
    fenetre1.mainloop()
def fbtn2_1():
    global condition,btn2_1,nb_equipe
    if nb_equipe<8:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn3_1['text'], btn3_2['text']])
    equipes=tkinter.mainloop()
    btn2_1=tkinter.Button(canvas, text=equipe_choisi, command=fbtn2_1, bg=color, width=15, height=1 )
    canvas.create_window(450, 175, window=btn2_1)
    fenetre1.mainloop()
def fbtn2_2():
    global condition,btn2_2,nb_equipe
    if nb_equipe<8:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn3_3['text'], btn3_4['text']])
    equipes=tkinter.mainloop()
    btn2_2=tkinter.Button(canvas, text=equipe_choisi, command=fbtn2_2, bg=color, width=15, height=1 )
    canvas.create_window(450, 475, window=btn2_2)
    fenetre1.mainloop()
def fbtn2_3():
    global condition,btn2_3,nb_equipe
    if nb_equipe<8:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn3_5['text'], btn3_6['text']])
    equipes=tkinter.mainloop()
    btn2_3=tkinter.Button(canvas, text=equipe_choisi, command=fbtn2_3, bg=color, width=15, height=1 )
    canvas.create_window(850, 175, window=btn2_3)
    fenetre1.mainloop()
def fbtn2_4():
    global condition,btn2_4,nb_equipe
    if nb_equipe<8:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn3_7['text'], btn3_8['text']])
    equipes=tkinter.mainloop()
    btn2_4=tkinter.Button(canvas, text=equipe_choisi, command=fbtn2_4, bg=color, width=15, height=1 )
    canvas.create_window(850, 475, window=btn2_4)
    fenetre1.mainloop()
def fbtn3_1():
    global condition,btn3_1,nb_equipe
    if nb_equipe<16:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn3_1['text'], btn3_2['text']])
    equipes=tkinter.mainloop()
    btn3_1=tkinter.Button(canvas, text=equipe_choisi, command=fbtn3_1, bg=color, width=15, height=1 )
    canvas.create_window(250, 100, window=btn3_1)
    fenetre1.mainloop()
def fbtn3_2():
    global condition,btn3_2,nb_equipe
    if nb_equipe<16:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn3_3['text'], btn3_4['text']])
    equipes=tkinter.mainloop()
    btn3_2=tkinter.Button(canvas, text=equipe_choisi, command=fbtn3_2, bg=color, width=15, height=1 )
    canvas.create_window(250, 250, window=btn3_2)
    fenetre1.mainloop()
def fbtn3_3():
    global condition,btn3_3,nb_equipe
    if nb_equipe<16:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn3_5['text'], btn3_6['text']])
    equipes=tkinter.mainloop()
    btn3_3=tkinter.Button(canvas, text=equipe_choisi, command=fbtn3_3, bg=color, width=15, height=1 )
    canvas.create_window(250, 400, window=btn3_3)
    fenetre1.mainloop()
def fbtn3_4():
    global condition,btn3_4,nb_equipe
    if nb_equipe<16:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn3_7['text'], btn3_8['text']])
    equipes=tkinter.mainloop()
    btn3_4=tkinter.Button(canvas, text=equipe_choisi, command=fbtn3_4, bg=color, width=15, height=1 )
    canvas.create_window(250, 550, window=btn3_4)
    fenetre1.mainloop()
def fbtn3_5():
    global condition,btn3_5,nb_equipe
    if nb_equipe<16:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn4_9['text'], btn4_10['text']])
    equipes=tkinter.mainloop()
    btn3_5=tkinter.Button(canvas, text=equipe_choisi, command=fbtn3_5, bg=color, width=15, height=1 )
    canvas.create_window(1050, 100, window=btn3_5)
    fenetre1.mainloop()
def fbtn3_6():
    global condition,btn3_6,nb_equipe
    if nb_equipe<16:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn4_11['text'], btn4_12['text']])
    equipes=tkinter.mainloop()
    btn3_6=tkinter.Button(canvas, text=equipe_choisi, command=fbtn3_6, bg=color, width=15, height=1 )
    canvas.create_window(1050, 250, window=btn3_6)
    fenetre1.mainloop()
def fbtn3_7():
    global condition,btn3_7,nb_equipe
    if nb_equipe<16:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn4_13['text'], btn4_14['text']])
    equipes=tkinter.mainloop()
    btn3_7=tkinter.Button(canvas, text=equipe_choisi, command=fbtn3_7, bg=color, width=15, height=1 )
    canvas.create_window(1050, 400, window=btn3_7)
    fenetre1.mainloop()
def fbtn3_8():
    global condition,btn3_8,nb_equipe
    if nb_equipe<16:
        choisir_equipe(lst_equipe)
    else:
        choisir_equipe([btn4_15['text'], btn4_16['text']])
    equipes=tkinter.mainloop()
    btn3_8=tkinter.Button(canvas, text=equipe_choisi, command=fbtn3_8, bg=color, width=15, height=1 )
    canvas.create_window(1050, 550, window=btn3_8)
    fenetre1.mainloop()
def fbtn4_1():
    global condition,btn4_1
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_1=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_1, bg=color, width=15, height=1 )
    canvas.create_window(50, 50, window=btn4_1)
    fenetre1.mainloop()
def fbtn4_2():
    global condition,btn4_2
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_2=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_2, bg=color, width=15, height=1 )
    canvas.create_window(50, 150, window=btn4_2)
    fenetre1.mainloop()
def fbtn4_3():
    global condition,btn4_3
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_3=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_3, bg=color, width=15, height=1 )
    canvas.create_window(50, 200, window=btn4_3)
    fenetre1.mainloop()
def fbtn4_4():
    global condition,btn4_4
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_4=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_4, bg=color, width=15, height=1 )
    canvas.create_window(50, 300, window=btn4_4)
    fenetre1.mainloop()
def fbtn4_5():
    global condition,btn4_5
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_5=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_5, bg=color, width=15, height=1 )
    canvas.create_window(50, 350, window=btn4_5)
    fenetre1.mainloop()
def fbtn4_6():
    global condition,btn4_6
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_6=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_6, bg=color, width=15, height=1 )
    canvas.create_window(50, 450, window=btn4_6)
    fenetre1.mainloop()
def fbtn4_7():
    global condition,btn4_7
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_7=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_7, bg=color, width=15, height=1 )
    canvas.create_window(50, 500, window=btn4_7)
    fenetre1.mainloop()
def fbtn4_8():
    global condition,btn4_8
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_8=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_8, bg=color, width=15, height=1 )
    canvas.create_window(50, 600, window=btn4_8)
    fenetre1.mainloop()
def fbtn4_9():
    global condition,btn4_9
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_9=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_9, bg=color, width=15, height=1 )
    canvas.create_window(1250, 50, window=btn4_9)
    fenetre1.mainloop()
def fbtn4_10():
    global condition,btn4_10
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_10=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_10, bg=color, width=15, height=1 )
    canvas.create_window(1250, 150, window=btn4_10)
    fenetre1.mainloop()
def fbtn4_11():
    global condition,btn4_11
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_11=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_11, bg=color, width=15, height=1 )
    canvas.create_window(1250, 200, window=btn4_11)
    fenetre1.mainloop()
def fbtn4_12():
    global condition,btn4_12
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_12=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_12, bg=color, width=15, height=1 )
    canvas.create_window(1250, 300, window=btn4_12)
    fenetre1.mainloop()
def fbtn4_13():
    global condition,btn4_13
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_13=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_13, bg=color, width=15, height=1 )
    canvas.create_window(1250, 350, window=btn4_13)
    fenetre1.mainloop()
def fbtn4_14():
    global condition,btn4_14
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_14=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_14, bg=color, width=15, height=1 )
    canvas.create_window(1250, 450, window=btn4_14)
    fenetre1.mainloop()
def fbtn4_15():
    global condition,btn4_15
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_15=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_15, bg=color, width=15, height=1 )
    canvas.create_window(1250, 500, window=btn4_15)
    fenetre1.mainloop()
def fbtn4_16():
    global condition,btn4_16
    choisir_equipe(lst_equipe)
    equipes=tkinter.mainloop()
    btn4_16=tkinter.Button(canvas, text=equipe_choisi, command=fbtn4_16, bg=color, width=15, height=1 )
    canvas.create_window(1250, 600, window=btn4_16)
    fenetre1.mainloop()

def window_gestion_matchs():
    """Fonction principale pour créer la fenêtre et le canvas pour le tournoi"""
    global fenetre1, canvas, nb_equipe, lst_equipedata
    # Connexion à la base de données SQLite
    connexion = sqlite3.connect('dbprojet.db')
    connexion.execute('PRAGMA foreign_keys= ON')

    # Récupération de la liste des équipes depuis la base de données
    lst_equipe_t = connexion.execute(f"SELECT nom_equipe FROM Equipe")
    lst_equipe = [i[0] for i in lst_equipe_t][1:]
    print(lst_equipe)
    connexion.close()

    # Initialisation des variables
    lst_equipedata=lst_equipe.copy()
    if len(lst_equipe) < 5:
        nb_equipe = 4
    elif len(lst_equipe) < 9:
        nb_equipe = 8
    else:
        nb_equipe = 16
    equipe_choisi=None
    condition=False
    
    fenetre1=tkinter.Tk()
    fenetre1.title ("GTS | Gestionnaire de tournoi")
    fenetre1.resizable(width=False,height=False)
    canvas=tkinter.Canvas(fenetre1, width=1300, height=650, bg="white")
    draw_samples(canvas)
    canvas.pack()

    btnreactualiser=tkinter.Button(canvas, text="reinitialiser", command=reactualiser, width=15, height=1 )
    canvas.create_window(650, 590, window=btnreactualiser)
    btnquitter=tkinter.Button(canvas, text="quitter", command=quitter, width=15, height=1 )
    canvas.create_window(650, 620, window=btnquitter)

    reactualiser()
    fenetre1.mainloop()


# Vérification si le script est exécuté en tant que programme principal
if __name__ == '__main__':
    # Appel de la fonction principale pour créer la fenêtre de gestion des matchs
    window_gestion_matchs()

"""
                    oo _,        ,
                  _((-'_/       |
                 /      "",   _//__,     __/
               ,',O`  | #  ",/.---'     /,-'
             ,'    __/-.   # "       \.//
        |`._(;_,--'     \     "       ||
         `-.\           ,\ #   ",     ||
           _\\         || \   #  "    ||
         /,-.`\       //   \      "  //
        //   `.`.    ||   | \ #  # ",/
        |      `.`._// \ //  \     # ",
                 `-.:\  ||    \  #     ",
      \`.___     // \:\ \\     \  #  #   ",
    ,--'----.\  ||   \:\ ||   //\         #",
             \\//    | | ||  //  \#    #     ",_
              |:|    |:|  \\//    \  #    #     `--.__
             /:/     |:|   \\      \     #    #     # `--._
            / /      | |    ||     |#      #          #    `-.
    \-     /:/    /| | |    ||     ;   #       #   #        # \
     \\__ |:|   _//  |:|    ||    :     #   #       #   #      \
      `-.\| |  /,-   |:|   //     :  #    #      #     #  /  #  :
         \ :| //     |:|  //_     : # |  #               /    # |\
          \:|//      \ \_/ / \\   :   :        |#     # | #     |\\
          |: /    /|  \ ::/   `    \ # \   #   |  #     |      #| \\
          / / \_ //   | : |         \  #\#  #  |       ,|  #    | ||
         | |    ||   / ::/           |   \     |# __,.' \    #  | ||
         |:|     \\ /:: /            | #/ |#  /-'' `. #  \     #; ||
          \:\     \\:::/             |  | |  |       \  # \ #  /  ||
          | |     / ::/              |  | | #|        |  | |  /   ||
          | |     | : |              |# | |  |        |  | |# |   ;; 
          |:|    / ::/               |  | | #|        |# | |  |  .;;.
           \:\   |:::|               |  | |  |        |  | | #|  ;;;;
           |:|  /:: /                | #| |  |        | #| |  |  ;;;;
           |:| / : / _  _            |  | |# |  __   _(  ) (  )  ;;;;
           | |\|   |/ //  _    _    _(  ) (  ) _ \\\//| / \| /  _ _   
           | :\|:: |/ ////_ _  _\\ / _\ |  \ | _\ \|//| |\\| |\/// _ __
            \ :| :_|/ /// _  \\ \\|///| |  | |_\\\\ |/| |\\| |\//////_
          _ | _  / |_//     \\ \\\|///| |  | | \\ \||_| |_\|_|\/ // /
       _  _\ _ \//:/  _  _ _ \_\_\\/_/| |  | | __   _ \\\ \/ |/  _  
     _  \\ \\ \|||/_ / _  \ \/// _  _\\_|  | |  _  _ \\ \|||/ //  _
       \\ \\\\\_|\\\//_ \\\ \|/_  \\ \\\\\//_|//   _\\\ \\||/ ////_
     claude\ \_\_\\\|////\ _\\\|//\\ \\\ \| // / ///_\ \\\|| /// 
             _\\\\\ |/////_ \|/// _\\ \\\||// ///__   \\ \\|// /
              \\\ \||/ ///         \ \\ \|// // //
"""