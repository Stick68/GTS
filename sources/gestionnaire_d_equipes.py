# Créé par Gauthier
import tkinter
from tkinter import ttk
from tkinter import messagebox
import sqlite3
import joueur

# Créé par Quentin, Aréna
# Vérifie si un ID existe déjà dans la base de données
def verifid(n):
    connexion = sqlite3.connect('dbprojet.db')
    connexion.execute('PRAGMA foreign_keys= ON')
    present = connexion.execute('SELECT id_equipe FROM Equipe')

    for id in present:
        if n == id[0]:
            connexion.close()
            return False
    connexion.close()
    return True

# Créé par Quentin
# Crée un nouvel ID unique pour un equipe
def creer_id():
    id = 1
    while not verifid(id):
        id += 1
    return id

# Créé par Quentin
# Ajoute un equipe à la base de données et à la liste locale
def add_team():
    connexion = sqlite3.connect('dbprojet.db')
    connexion.execute('PRAGMA foreign_keys= ON')

    id = creer_id()
    nom = name_entry.get()

    nouv_equipe = joueur.Equipe(id, nom)
    liste_team.append(nouv_equipe)

    connexion.execute(f"INSERT INTO Equipe VALUES {str(nouv_equipe.get_sql())}")
    print(nouv_equipe.get_sql())

    connexion.commit()
    connexion.close()
    # update_list_team()
    update_list_select_team()

# Créé par Gauthier, Quentin, David
# Fonction pour tester et confirmer la suppression d'un equipe
def test_delete():
    alert = messagebox.askquestion("Attention", f"Voulez-vous supprimer cette equipe : {select_equipe_combobox.selection_get()[:-1]} ?")
    if alert == "yes":
        pass
    else:
        return

    selart = select_equipe_combobox.selection_get()
    print(selart)
    liste_equipe_delete = selart.split()
    print(liste_equipe_delete)
    id_equipe = int(liste_equipe_delete[0][3:])
    print(id_equipe)
    for i in liste_team:
        if i.id == id_equipe:
            liste_team.remove(i)
            connexion = sqlite3.connect('dbprojet.db')
            connexion.execute('PRAGMA foreign_keys= ON')

            connexion.execute(f"DELETE FROM Equipe WHERE id_equipe = {i.id}")
            connexion.commit()
            connexion.close()
            # update_list_player()
            update_list_select_team()

# Créé par Gauthier, Quentin
def test_add_equipe_joueur():
    selart = equipe_liste.selection_get()
    selart2 = select_equipe_combobox.get()
    liste_joueur_edit = selart.split()
    id_joueur = int(liste_joueur_edit[0][3:])
    id_equipe = choix_equipe
    for i in liste_player:
        if i.get_id() == id_joueur:
            connexion = sqlite3.connect('dbprojet.db')
            connexion.execute('PRAGMA foreign_keys= ON')
            print(id_equipe)
            i.equipe = id_equipe
            print(i.get_sql())
            connexion.execute(f"UPDATE Joueur SET id_equipe={id_equipe} WHERE id = {i.get_id()}")
            connexion.commit()
            connexion.close()
            update_list_player()

# Créé par Gauthier
def test_choix_2(event):
    global choix_equipe
    choix_equipe = int(select_equipe_combobox.get().split()[0][3:])

# Créé par Gauthier et modifié par Quentin
# Met à jour la liste des joueurs dans l'interface
def update_list_player():
    equipe_liste.delete(0, tkinter.END)
    for player in liste_player:
        test = f'ID={player.get_id()} {player.prenom} {player.nom.upper()} {player.equipe}'
        equipe_liste.insert(tkinter.END, test)

# Créé par Gauthier et modifié par Quentin
def update_list_select_team():
    liste_select_equipe = [f"ID={equ.id} {equ.nom_equipe}" for equ in liste_team[1:]]
    select_equipe_combobox.configure(values=liste_select_equipe)
    print(liste_select_equipe)

# Créé par Quentin
# Initialisation de la liste des equipes à partir de la base de données
liste_team = []
liste_player = []
co_init = sqlite3.connect('dbprojet.db')
co_init.execute('PRAGMA foreign_keys= ON')
liste_equ_init = co_init.execute('SELECT * FROM Equipe')
for equ in liste_equ_init:
    nouv_equ = joueur.Equipe(equ[0], equ[1])
    liste_team.append(nouv_equ)
liste_jou_init = co_init.execute('SELECT * FROM Joueur')
for jou in liste_jou_init:
    nouv_jou = joueur.Joueur(jou[0], jou[1], jou[2], jou[3])
    liste_player.append(nouv_jou)
co_init.close()

# Créé par Gauthier
def window_gestion_equipe():
    """Fenêtre principale"""

    global equipe_liste, name_entry, prenom_entry, equipe_entry, select_equipe_combobox, choix_equipe
    # Interface utilisateur Tkinter
    main_window = tkinter.Tk()
    main_window.title("GTS | Gestionnaire d'équipe")
    main_window.geometry("500x400")
    main_window.resizable(width=False, height=False)

    main_window_style = ttk.Style()
    main_window_style.theme_use("default")

    main_window_frame = tkinter.Frame(main_window)
    main_window_frame.pack()

    # Cadre pour ajouter un nouveau equipe
    add_new_equipe = tkinter.LabelFrame(main_window_frame, text="Ajouter une nouvelle équipe")
    add_new_equipe.grid(row=0, column=0)

    name_label = tkinter.Label(add_new_equipe, text="Nom")
    name_label.grid(row=0, column=0)

    name_entry = tkinter.Entry(add_new_equipe)
    name_entry.grid(row=1, column=0)

    select_equipe = tkinter.Label(add_new_equipe, text="Sélection équipe")
    select_equipe.grid(row=0, column=1)

    liste_select_equipe = [f"ID={equ.id} {equ.nom_equipe}" for equ in liste_team[1:]]
    select_equipe_combobox = ttk.Combobox(add_new_equipe, values=liste_select_equipe)
    select_equipe_combobox.grid(row=1, column=1)
    select_equipe_combobox.bind('<<ComboboxSelected>>', test_choix_2)
    choix_equipe = 0

    btn = tkinter.Button(add_new_equipe, text="Ajouter", width=15, command=add_team)
    # btn = tkinter.Button(add_new_equipe, text="Ajouter", width=15, command=update_list_select_team)
    btn.grid(row=2, column=0)

    # Cadre pour afficher la liste des equipes
    team = tkinter.LabelFrame(main_window_frame, text="Les participants")
    team.grid(row=1, column=0)

    equipe_liste_label = tkinter.Label(team, text="Participants")
    equipe_liste_label.grid(row=0, column=0)

    equipe_liste = tkinter.Listbox(team, width=30, selectmode='multiple')
    update_list_player()
    equipe_liste.grid(row=1, column=0)

    scrollbar = tkinter.Scrollbar(team, orient="vertical", command=equipe_liste.yview)
    scrollbar.grid(row=1, column=1, sticky="ns")
    equipe_liste.configure(yscrollcommand=scrollbar.set)

    # Cadre pour supprimer un equipe
    delete_team_label = tkinter.Label(team, text="(Equipe 0 = pas d'équipe)")
    delete_team_label.grid(row=0, column=2)

    btn1 = tkinter.Button(add_new_equipe, text="Supprimer", command=test_delete, width=15)
    btn1.grid(row=2, column=1)

    btn2 = tkinter.Button(team, text="Ajouter le participant à l'équipe", command=test_add_equipe_joueur)
    btn2.grid(row=1, column=2)

    # Configuration des marges pour tous les widgets
    for widget in add_new_equipe.winfo_children():
        widget.grid_configure(padx=10, pady=5)

    for widget in team.winfo_children():
        widget.grid_configure(padx=10, pady=5)

    main_window.mainloop()

if __name__ == '__main__':
    window_gestion_equipe()
