# Créé par Gauthier et modifié par Quentin
import tkinter
from tkinter import ttk
from tkinter import messagebox
import sqlite3
import joueur
import visualiseur_de_statistiques
import visualiseur_de_statistiques

# Créé par Quentin, Aréna
# Vérifie si un ID existe déjà dans la base de données
def verifid(n):
    connexion = sqlite3.connect('dbprojet.db')
    connexion.execute('PRAGMA foreign_keys= ON')
    present = connexion.execute('SELECT id FROM Joueur')

    for id in present:
        if n == id[0]:
            connexion.close()
            return False
    connexion.close()
    return True

# Créé par Quentin
# Créer un nouvel ID unique pour un joueur
def creer_id():
    id = 1
    while not verifid(id):
        id += 1
    return id

# Créé par Quentin
# Ajoute un joueur à la base de données et à la liste locale
def add_player():
    connexion = sqlite3.connect('dbprojet.db')
    connexion.execute('PRAGMA foreign_keys= ON')

    id = creer_id()
    nom = name_entry.get()
    prenom = prenom_entry.get()
    equipe = "0"

    nouv_joueur = joueur.Joueur(id, nom, prenom, equipe)
    liste_player.append(nouv_joueur)

    connexion.execute(f"INSERT INTO Joueur VALUES {str(nouv_joueur.get_sql())}")
    print(nouv_joueur.get_sql())

    connexion.commit()
    connexion.close()
    update_list_player()

# Créé par Gauthier, Quentin, David
# Fonction pour tester et confirmer la suppression d'un joueur
def test_delete():
    alert = messagebox.askquestion("Attention", f"Voulez-vous supprimer ce joueur : {player_liste.selection_get()[:-1]} ?")
    if alert == "yes":
        pass
    else:
        return

    selart = player_liste.selection_get()
    liste_joueur_delete = selart.split()
    print(liste_joueur_delete)
    id_joueur = int(liste_joueur_delete[0][3:])
    print(id_joueur)
    for i in liste_player:
        if i.get_id() == id_joueur:
            liste_player.remove(i)
            connexion = sqlite3.connect('dbprojet.db')
            connexion.execute('PRAGMA foreign_keys= ON')

            connexion.execute(f"DELETE FROM Joueur WHERE id = {i.get_id()}")
            connexion.commit()
            connexion.close()
            update_list_player()

# Créé par Quentin
def test_voire_stastisitques_joueur():
    """Affiche la fenêtre des statistiques de joueur"""
    visualiseur_de_statistiques.window_afficher_stats(player_liste.selection_get().split()[1:3])

# Créé par Gauthier et modifié par Quentin
# Met à jour la liste des joueurs dans l'interface
def update_list_player():
    player_liste.delete(0, tkinter.END)
    for player in liste_player:
        test = f'ID={player.get_id()} {player.prenom} {player.nom.upper()} {player.equipe}'
        player_liste.insert(tkinter.END, test)

# Créé par Quentin
# Initialisation de la liste des joueurs à partir de la base de données
liste_player = []

co_init = sqlite3.connect('dbprojet.db')
co_init.execute('PRAGMA foreign_keys= ON')
liste_jou_init = co_init.execute('SELECT * FROM Joueur')
for jou in liste_jou_init:
    nouv_jou = joueur.Joueur(jou[0], jou[1], jou[2], jou[3])
    liste_player.append(nouv_jou)
co_init.close()

# Créé par Gauthier
def window_gestion_tournoi():
    global player_liste, name_entry, prenom_entry, equipe_entry
    # Interface utilisateur Tkinter
    main_window = tkinter.Tk()
    main_window.title("GTS | Gestionnaire de participants")
    main_window.geometry("500x400")
    main_window.resizable(width=False, height=False)

    main_window_style = ttk.Style()
    main_window_style.theme_use("default")

    main_window_frame = tkinter.Frame(main_window)
    main_window_frame.pack()

    # Cadre pour ajouter un nouveau joueur
    add_new_equipe = tkinter.LabelFrame(main_window_frame, text="Ajouter un nouveau participant")
    add_new_equipe.grid(row=0, column=0)

    name_label = tkinter.Label(add_new_equipe, text="Nom")
    name_label.grid(row=0, column=0)

    prenom_label = tkinter.Label(add_new_equipe, text="Prénom")
    prenom_label.grid(row=0, column=1)

    name_entry = tkinter.Entry(add_new_equipe)
    name_entry.grid(row=1, column=0)

    prenom_entry = tkinter.Entry(add_new_equipe)
    prenom_entry.grid(row=1, column=1)

    btn = tkinter.Button(add_new_equipe, text="Valider", width=15, command=add_player)
    btn.grid(row=2, column=1)

    # Cadre pour afficher la liste des joueurs
    player = tkinter.LabelFrame(main_window_frame, text="Les participants")
    player.grid(row=1, column=0)

    player_liste_label = tkinter.Label(player, text="Participants")
    player_liste_label.grid(row=0, column=0)

    player_liste = tkinter.Listbox(player, width=30)
    update_list_player()
    player_liste.grid(row=1, column=0)

    scrollbar = tkinter.Scrollbar(player, orient="vertical", command=player_liste.yview)
    scrollbar.grid(row=1, column=1, sticky="ns")
    player_liste.configure(yscrollcommand=scrollbar.set)

    # Cadre pour supprimer un joueur
    delete_player_label = tkinter.Label(player, text="Supprimer un participant")
    delete_player_label.grid(row=0, column=2)

    btn1 = tkinter.Button(player, text="Supprimer", command=test_delete, width=15)
    btn1.grid(row=1, column=2)
    
    btn1 = tkinter.Button(player, text="Statistiques", command=test_voire_stastisitques_joueur, width=15)
    btn1.grid(row=2, column=2)

    # Configuration des marges pour tous les widgets
    for widget in add_new_equipe.winfo_children():
        widget.grid_configure(padx=10, pady=5)

    for widget in player.winfo_children():
        widget.grid_configure(padx=10, pady=5)

    main_window.mainloop()

if __name__ == '__main__':
    window_gestion_tournoi()
