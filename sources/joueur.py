# Créé par Timéo
class Joueur:
    """Classe représentant un joueur."""

    def __init__(self, identifiant, nom, prenom, equipe):
        """
        Initialise un joueur avec un nom, un prénom, un identifiant et une équipe.

        Paramètres :
        - nom (str) : Le nom de famille du joueur.
        - prenom (str) : Le prénom du joueur.
        - identifiant (str) : L'identifiant unique du joueur.
        - equipe (str) : L'équipe à laquelle le joueur appartient.
        """

        self.__id = identifiant
        self.nom = nom
        self.prenom = prenom
        self.equipe = equipe
        self.score = 0

    def set_score(self, score):
        """
        Définit le score d'un joueur.

        Paramètres :
        - score (int) : Le score à définir pour le joueur.
        """
        self.score = score

    def get_id(self):
        """
        Récupère l'identifiant du joueur.

        Retourne :
        - str : L'identifiant du joueur.
        """
        return self.__id
    
    def get_sql(self):
        return (self.__id, self.nom, self.prenom, self.equipe)

# Créé par Timéo
class Equipe:
    """Classe représentant une équipe."""

    def __init__(self, identifiant, nom_equipe):
        """
        Initialise une équipe avec un nom d'équipe.

        Paramètres :
        - nom_equipe (str) : Le nom de l'équipe.
        """
        self.nom_equipe = nom_equipe
        self.id = identifiant
        self.joueurs_id = []
        self.joueurs_nom = []
        self.score = 0

    def set_joueur(self, liste_joueurs):
        """
        Ajoute des joueurs à l'équipe en fonction du nom de l'équipe.

        Paramètres :
        - liste_joueurs (list) : Liste d'objets joueur.
        """
        for joueur in liste_joueurs:
            if joueur.equipe == self.nom_equipe:
                self.joueurs_nom.append(joueur.nom)
                self.joueurs_id.append(joueur.get_id())

    def set_score(self, liste_joueurs):
        """
        Calcule et définit le score total de l'équipe en fonction des scores individuels des joueurs.

        Paramètres :
        - liste_joueurs (list) : Liste d'objets joueur.
        """
        for joueur in liste_joueurs:
            if joueur.equipe == self.nom_equipe:
                self.score += joueur.score
        
    def get_sql(self):
        return (self.id, self.nom_equipe)


# Exemple d'utilisation :
"""
joueur1 = Joueur('andrianarivony', 'arena', 'joueur1', 'Lilas')
joueur2 = Joueur('de oliveira', 'timeo', 'joueur2', 'Lilas')
joueur3 = Joueur('steiner', 'quentin', 'joueur3', 'Bleu')
joueur4 = Joueur('schirmer', 'david', 'joueur4', 'Bleu')
joueur5 = Joueur('strich', 'gautier', 'joueur5', 'Lilas')
joueur1.set_score(5)
joueur2.set_score(10)
joueur5.set_score(1)

Lilas = Equipe('Lilas')
Bleu = Equipe('Bleu')
Lilas.set_joueur(liste_joueurs)
Bleu.set_joueur(liste_joueurs)
print(Lilas.joueurs_nom)
Lilas.set_score(liste_joueurs)
print(Lilas.score)
"""